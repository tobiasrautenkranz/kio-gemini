# KIO Gemini
Provides transparent access for KDE to files using the Gemini protocol.

You probably want to use this in conjunction with the geminipart and Konqueror
for browsing and maybe Akregator for fetching atom feeds.

Supports most of the Gemini Protocol with the exception of client certificates
and certificate trust (TOFU); i.e.: All certificates are accepted.

# Installation

After cloning the repository you can build and install it with:

```sh
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DKDE_INSTALL_USE_QT_SYS_PATHS=TRUE ..
make
sudo make install
kdeinit5
```

Afterwards you can use gemini:// URLs in the KDE applications. 
You may test is from the command line with e.g.:

``` sh
kioclient5 "cat" gemini://geminiprotocol.net/
```
