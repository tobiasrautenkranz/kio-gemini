// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QObject>
#include <QSslSocket>
#include <kio/workerbase.h>
#include <optional>

class QSslError;

/** Provides access to the Gemini Protocol for KIO.
 *
 * Metadata:
 *
 * permanent-redirect: true when a permenent redirection is encountered
 * charset: the charset for text/
 * content-language: the language of text/gemini (when provided)
 *
 * Gemini input (Status code 1x) support can be enabled setting
 * inputSupported for the jobs metadata. When a input response is received
 * the input metadata is set and will contain the prompt. */
class Gemini : public QObject, public KIO::WorkerBase
{
    Q_OBJECT
public:
    Gemini(const QByteArray &pool, const QByteArray &app);

    void worker_status() override;

    virtual KIO::WorkerResult get(const QUrl &url) override;
    Q_REQUIRED_RESULT virtual KIO::WorkerResult mimetype(const QUrl &url) override;

private:
    virtual KIO::WorkerResult write(const QByteArray &data) override;

    /** Get an process the header.
     * Returns a result when the request has finished due to an error or redirection.
     * Nothing is returned if the request was sucessfull and now the data may be fetched.
     */
    Q_REQUIRED_RESULT std::optional<KIO::WorkerResult> processHeader(const QUrl &url);

    /** Opens the connection tho the server */
    Q_REQUIRED_RESULT KIO::WorkerResult openUrl(const QUrl &url);

    /** Writes data to the socket adding a CRLF */
    Q_REQUIRED_RESULT KIO::WorkerResult writeLine(const QByteArray &data);

    /** Reads a CRLF terminated line from the socket and
     * returns the line or a null QString */
    QString readLine();

    /** Reads the available data from the socket and sends it to the job
     * Returns true on success */
    bool readData();

    /** Hold data that was read from the socket but not processed.
     * e.g. read beyond a line end. */
    QByteArray mBuffer;

    /** Writes a Gemini request for url to the socket */
    Q_REQUIRED_RESULT KIO::WorkerResult sendRequest(const QUrl &url);
    struct Header {
        int code[2];
        QString meta;
    };

    // TODO use std::expected instead.
    struct HeaderResult {
        HeaderResult(Header h, KIO::WorkerResult r)
            : header(h)
            , result(r)
        {
        }

        Header header = {};
        KIO::WorkerResult result = KIO::WorkerResult::pass();
    };

    /** Reads the Gemini Header from the socket */
    HeaderResult getHeader();

    /** Counts consecutive redirects */
    unsigned int mRedirectCount = 0;

    KIO::WorkerResult connectToHost(const QString &host, quint16 port);
    bool isConnected();
    void disconnectFromHost();
    ssize_t socketWrite(const char *data, ssize_t len);
    ssize_t socketRead(char *data, ssize_t len);

    /** the underlying socket */
    QSslSocket mSocket;

private Q_SLOTS:
    void onSslErrors(const QList<QSslError> &errors);
};
