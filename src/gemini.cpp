// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "gemini.h"
#include "gemini-debug.h"

#include <KF5/KIOCore/kio/global.h>
#include <KLocalizedString>
#include <KSslCertificateManager>
#include <QAbstractSocket>
#include <QCoreApplication>
#include <QDebug>
#include <QSslCipher>
#include <QSslSocket>
#include <cstddef>

static const QByteArray crlf = QByteArrayLiteral("\r\n");
static constexpr int gemini_default_port = 1965;

static constexpr size_t gemini_median_file_size = 1500; // gemini://gemini.bortzmeyer.org/software/lupa/stats.gmi

static constexpr bool ignoreSslCertErrors = true;

static int socketErrorToKIOError(QAbstractSocket::SocketError error)
{
    switch (error) {
    case QAbstractSocket::UnsupportedSocketOperationError:
        return KIO::ERR_UNSUPPORTED_ACTION;
    case QAbstractSocket::RemoteHostClosedError:
        return KIO::ERR_CONNECTION_BROKEN;
    case QAbstractSocket::SocketTimeoutError:
        return KIO::ERR_SERVER_TIMEOUT;
    case QAbstractSocket::HostNotFoundError:
        return KIO::ERR_UNKNOWN_HOST;
    default:
        return KIO::ERR_CANNOT_CONNECT;
    }
}

// https://invent.kde.org/frameworks/kio/-/blob/master/docs/metadata.txt
//
// export QT_LOGGING_RULES="kf.kio.gemini=true"

class KIOPluginForMetaData : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kio.worker.gemini" FILE "gemini.json")
};

extern "C" {
int Q_DECL_EXPORT kdemain(int argc, char **argv)
{
    QCoreApplication app(argc, argv); // needed for password server
    app.setApplicationName(QStringLiteral("kio_gemini"));

    qInfo() << "Launching KIO Worker.";
    if (argc != 4) {
        fprintf(stderr, "Usage: kio_gemini protocol domain-socket1 domain-socket2\n");
        exit(-1);
    }
    Gemini worker(argv[2], argv[3]);
    worker.dispatchLoop();
    return 0;
}
}

Gemini::Gemini(const QByteArray &pool, const QByteArray &app)
    : WorkerBase("Gemini", pool, app)
{
    mBuffer.reserve(2 * gemini_median_file_size);
}

void Gemini::worker_status()
{
    workerStatus(mSocket.peerName(), isConnected());
}

KIO::WorkerResult Gemini::write(const QByteArray &data)
{
    qCDebug(KIO_GEMINI) << "write" << data;
    qsizetype bytesWritten = 0;
    while (bytesWritten < data.length()) {
        auto r = socketWrite(data.constData() + bytesWritten, data.length() - bytesWritten);
        if (r < 0) {
            return KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, mSocket.peerName());
        }
        bytesWritten += r;
    }

    return KIO::WorkerResult::pass();
}

KIO::WorkerResult Gemini::writeLine(const QByteArray &data)
{
    // prevents errors in servers that do not wait for the second package
    return write(data + crlf);
    /*
     // do not use this
    write(data);
    write(crlf);
    */
}

QString Gemini::readLine()
{
    constexpr qsizetype chunkSize = 1024;
    auto totalBytesRead = mBuffer.size();

    while (true) {
        if (auto i = mBuffer.indexOf(crlf); i >= 0) {
            qCDebug(KIO_GEMINI) << "found line with length: " << i;
            auto line = QString::fromUtf8(mBuffer.constData(), i);
            mBuffer.remove(0, i + crlf.size());

            return line;
        }

        mBuffer.resize(totalBytesRead + chunkSize);
        const auto n = socketRead(mBuffer.data() + totalBytesRead,
                                  mBuffer.size() - totalBytesRead);
        qCDebug(KIO_GEMINI) << "bytes read from socket: " << n;

        if (n <= 0) {
            return QString();
        }

        totalBytesRead += n;
        mBuffer.resize(totalBytesRead);
    }
}

bool Gemini::readData()
{
    qCDebug(KIO_GEMINI) << "read data";
    constexpr qsizetype chunkSize = 2 * gemini_median_file_size;
    auto totalSize = mBuffer.size();
    if (mBuffer.size() > 0) {
        processedSize(totalSize);
        data(mBuffer);
    }
    mBuffer.resize(chunkSize);
    ssize_t ret;
    while ((ret = socketRead(mBuffer.data(), mBuffer.length())) > 0) {
        qCDebug(KIO_GEMINI) << "read" << ret;
        totalSize += ret;
        processedSize(totalSize);
        mBuffer.resize(ret);
        data(mBuffer);
        mBuffer.resize(chunkSize);
    }

    return ret != 0;
}

// temporarily add self signed certificates
void Gemini::onSslErrors(const QList<QSslError> &sslErrors)
{
    if (!KSslCertificateManager::nonIgnorableErrors(sslErrors).isEmpty()) {
        // can't fix
        return;
    }

    const auto certManager = KSslCertificateManager::self();
    auto rule = certManager->rule(mSocket.peerCertificateChain().first(), mSocket.peerVerifyName());
    if (!rule.filterErrors(sslErrors).isEmpty()) {
        qCDebug(KIO_GEMINI) << "ignoring ssl errors " << sslErrors;
        rule.setExpiryDateTime(QDateTime::currentDateTime().addSecs(2));
        rule.setIgnoredErrors(mSocket.sslHandshakeErrors());

        certManager->setRule(rule);
    }
}

KIO::WorkerResult Gemini::connectToHost(const QString &host, quint16 port)
{
    mSocket.setPeerVerifyName(host);

    const int timeout = (connectTimeout() * 1000); // 20 sec timeout value

    disconnectFromHost(); // Reset some state, even if we are already disconnected

    mSocket.ignoreSslErrors();
    mSocket.connectToHostEncrypted(host, port);
    mSocket.waitForEncrypted(timeout > -1 ? timeout : -1);

    if (mSocket.state() != QAbstractSocket::ConnectedState) {
        const auto errorString = host + QLatin1String(": ") + mSocket.errorString();
        return KIO::WorkerResult::fail(socketErrorToKIOError(mSocket.error()), errorString);
        ;
    }

    const auto cipher = mSocket.sessionCipher();
    if (!mSocket.isEncrypted() || mSocket.mode() != QSslSocket::SslClientMode || cipher.isNull() || cipher.usedBits() == 0
        || mSocket.peerCertificateChain().isEmpty()) {
        return KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN);
    }

    const auto sslErrors = mSocket.sslHandshakeErrors();

    return KIO::WorkerResult::pass();
}

bool Gemini::isConnected()
{
    return mSocket.state() == QAbstractSocket::ConnectedState;
}

void Gemini::disconnectFromHost()
{
    if (mSocket.state() == QAbstractSocket::UnconnectedState) {
        // discard incoming data - the remote host might have disconnected us in the meantime
        // but the visible effect of disconnectFromHost() should stay the same.
        mSocket.close();
        return;
    }

    mSocket.disconnectFromHost();
    if (mSocket.state() != QAbstractSocket::UnconnectedState) {
        mSocket.waitForDisconnected(-1); // wait for unsent data to be sent
    }
    mSocket.close(); // whatever that means on a socket
}

ssize_t Gemini::socketWrite(const char *data, ssize_t len)
{
    auto written = mSocket.write(data, len);

    bool success = mSocket.waitForBytesWritten(-1);

    if (mSocket.state() != QAbstractSocket::ConnectedState || !success) {
        return -1;
    }

    return written;
}

ssize_t Gemini::socketRead(char *data, ssize_t len)
{
    if (mSocket.mode() != QSslSocket::SslClientMode) {
        // qDebug() << "lost SSL connection.";
        return -1;
    }

    if (!mSocket.bytesAvailable()) {
        mSocket.waitForReadyRead(-1);
    }
    return mSocket.read(data, len);
}

KIO::WorkerResult Gemini::openUrl(const QUrl &url)
{
    mBuffer.clear();

    if (!url.userInfo().isEmpty()) {
        return KIO::WorkerResult::fail(KIO::ERR_CANNOT_CONNECT, i18n("User info part of URL not allowed by specification"));
    }

    auto port = url.port(gemini_default_port);
    infoMessage(QStringLiteral("connecting to: %1:%2").arg(url.host()).arg(port));
    qCDebug(KIO_GEMINI) << "connecting:" << url.host() << port;

    if (ignoreSslCertErrors) {
        connect(&mSocket, QOverload<const QList<QSslError> &>::of(&QSslSocket::sslErrors), this, &Gemini::onSslErrors);

        // mSocket.setPeerVerifyMode(QSslSocket::QueryPeer);
        // need to use VerifyNone to accept self signed certificates
        mSocket.setPeerVerifyMode(QSslSocket::VerifyNone);
    }

    if (!connectToHost(url.host(), port).success())
        return KIO::WorkerResult::fail();

    if (ignoreSslCertErrors) {
        auto certChain = mSocket.peerCertificateChain();
        if (certChain.size() > 1) {
            qCWarning(KIO_GEMINI) << "received a certificate chain instead of a self signed certificate";
        } else if (certChain.size() == 0) {
            return KIO::WorkerResult::fail(KIO::ERR_CANNOT_CONNECT, i18n("No Server certificate received"));
        }

        auto cm = KSslCertificateManager::self();
        auto rule = cm->rule(certChain.first(), url.host());

        auto sslErrors = QSslCertificate::verify(certChain, url.host());
        const QList<QSslError> remainingErrors = rule.filterErrors(sslErrors);

        QStringList errors;
        for (const QSslCertificate &cert : qAsConst(certChain)) {
            QStringList errorList;
            for (const QSslError &error : qAsConst(remainingErrors)) {
                if (error.certificate() == cert) {
                    errorList.append(QString::number(static_cast<int>(error.error())));
                }
            }
            errors.append(errorList.join(QLatin1Char('\t')));
        }
        setMetaData(QStringLiteral("ssl_cert_errors"), errors.join(QLatin1Char('\n')));
    }

    return KIO::WorkerResult::pass();
}

KIO::WorkerResult Gemini::sendRequest(const QUrl &url)
{
    infoMessage(QStringLiteral("sending request"));
    auto urlString = url.url().toUtf8();
    if (url.path().isEmpty()) {
        urlString += "/";
    }

    const auto result = writeLine(urlString);
    mSocket.flush();

    return result;
}

Gemini::HeaderResult Gemini::getHeader()
{
    infoMessage(QStringLiteral("waiting for response"));
    const auto response = readLine();
    infoMessage(QStringLiteral("header received"));
    qCDebug(KIO_GEMINI) << "response" << response << "\n\n";

    Header header;

    if (response.isNull()) {
        return {header, KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, i18n("socket error for response"))};
    }

    if (response.length() < 2) {
        return {header, KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid response"))};
    }

    if (!response[0].isDigit() || !response[1].isDigit()) {
        return {header, KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid header"))};
    }

    switch (response[0].digitValue()) {
    case 1:
    case 2:
    case 3:
        if (response.length() < 3 || response[2] != QLatin1Char(' ')) {
            return {header, KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid header"))};
        }

        return {{response[0].digitValue(), response[1].digitValue(), response.mid(3)}, KIO::WorkerResult::pass()};
        break;

    case 4:
    case 5:
    case 6:
        if (response.length() >= 3) {
            if (response[2] != QLatin1Char(' ')) {
                return {header, KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid header"))};
            }

            return {{response[0].digitValue(), response[1].digitValue(), response.mid(3)}, KIO::WorkerResult::pass()};
        }

        return {{response[0].digitValue(), response[1].digitValue(), {}}, KIO::WorkerResult::pass()};

        break;

    default:
        return {header, KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid header"))};
    }
}

KIO::WorkerResult Gemini::get(const QUrl &url)
{
    const auto result = processHeader(url);
    if (result) {
        return *result;
    }

    qCDebug(KIO_GEMINI) << "connected" << isConnected();
    if (!isConnected())
        return KIO::WorkerResult::fail();

    infoMessage(QStringLiteral("receiving data"));
    auto success = readData();
    if (!success) {
        qCDebug(KIO_GEMINI) << "read data error" << mSocket.errorString();
        disconnectFromHost();
        return KIO::WorkerResult::fail(KIO::ERR_CANNOT_READ, QLatin1String("read error: ") + mSocket.errorString());
    }

    disconnectFromHost();
    infoMessage(QStringLiteral("data received"));

    return KIO::WorkerResult::pass();
}

KIO::WorkerResult Gemini::mimetype(const QUrl &url)
{
    auto result = processHeader(url);
    return result ? *result : KIO::WorkerResult::pass();
}

Q_REQUIRED_RESULT std::optional<KIO::WorkerResult> Gemini::processHeader(const QUrl &url)
{
    qCDebug(KIO_GEMINI) << "Mimetype" << url.toDisplayString();
    auto result = openUrl(url);
    if (!result.success()) {
        return result;
    }

    result = sendRequest(url);
    if (!result.success()) {
        disconnectFromHost();
        return result;
    }

    const auto ret = getHeader();
    if (!ret.result.success()) {
        disconnectFromHost();
        return ret.result;
    }
    auto header = ret.header;

    setMetaData(QStringLiteral("responsecode"), QString::number(header.code[0] * 10 + header.code[1]));

    if (header.code[0] == 1) {
        if (header.code[1] == 1) {
            if (hasMetaData(QStringLiteral("no-auth"))) {
                disconnectFromHost();
                return KIO::WorkerResult::fail(KIO::ERR_ACCESS_DENIED, QStringLiteral("error authentication required"));
            }

            KIO::AuthInfo authInfo;
            authInfo.url = url;
            authInfo.prompt = header.meta;
            authInfo.setExtraField(QStringLiteral("hide-username-line"), true);
            // authInfo.setExtraField("bypass-cache-and-kwallet", true);
            authInfo.readOnly = true;
            authInfo.keepPassword = false;

            if (!checkCachedAuthentication(authInfo)) {
                auto errorCode = openPasswordDialog(authInfo);
                if (errorCode) {
                    disconnectFromHost();
                    switch (errorCode) {
                    case KIO::ERR_USER_CANCELED:
                        return KIO::WorkerResult::fail(errorCode, QStringLiteral("authentication canceled"));
                        break;
                    case KIO::ERR_PASSWD_SERVER:
                        // ERR_ACCESS_DENIED?? FIXME
                        return KIO::WorkerResult::fail(errorCode, QStringLiteral("internal error: communication with password server failed"));
                        break;
                    default:
                        return KIO::WorkerResult::fail(errorCode, QStringLiteral("error with authentication"));
                    }
                }
            }
            QUrl rUrl(url);
            rUrl.setQuery(authInfo.password);
            redirection(rUrl);
            disconnectFromHost();
            return KIO::WorkerResult::pass();
        } else {
            setMetaData(QStringLiteral("input"), header.meta);
            // send a "fake" mimetype such that we get the metadata
            mimeType(QStringLiteral("text/gemini"));
            if (!hasMetaData(QStringLiteral("inputSupported"))) {
                return KIO::WorkerResult::fail(KIO::ERR_UNSUPPORTED_PROTOCOL, QStringLiteral("Gemini INPUT"));
            }

            disconnectFromHost();
            return KIO::WorkerResult::pass();
        }
    }

    if (header.code[0] != 2) {
        switch (header.code[0]) {
        case 1:
            return KIO::WorkerResult::fail(KIO::ERR_UNSUPPORTED_PROTOCOL, QStringLiteral("Gemini INPUT"));
            break;
        case 3:
            qCDebug(KIO_GEMINI) << "redirect" << header.meta << " count: " << mRedirectCount;
            {
                QUrl redirectUrl(header.meta);
                if (!redirectUrl.isValid()) {
                    return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST, redirectUrl.toDisplayString());
                    break;
                }

                if (redirectUrl.isRelative()) {
                    redirectUrl = url.resolved(redirectUrl);
                }

                if (redirectUrl == url) {
                    disconnectFromHost();
                    return KIO::WorkerResult::fail(KIO::ERR_CYCLIC_LINK, redirectUrl.toDisplayString());
                } else {
                    mRedirectCount++;
                    if (mRedirectCount >= 5) {
                        auto result = messageBox(WarningContinueCancel, i18n("Redirection limit reached; continue anyway?"), i18n("Redirection"));
                        if (result != Continue) {
                            qCDebug(KIO_GEMINI) << "redirection canceled";
                            disconnectFromHost();
                            mRedirectCount = 0;
                            return KIO::WorkerResult::fail(KIO::ERR_USER_CANCELED, redirectUrl.toDisplayString());
                        } else {
                            qCDebug(KIO_GEMINI) << "redirection continue";
                        }
                    }
                    if (header.code[1] == 1) {
                        setMetaData(QStringLiteral("permanent-redirect"), QStringLiteral("true"));
                    }
                    redirection(redirectUrl);
                    disconnectFromHost();
                    return KIO::WorkerResult::pass();
                }
            }
            break;
        case 4:
            switch (header.code[1]) {
            case 1:
            case 2:
                return KIO::WorkerResult::fail(KIO::ERR_INTERNAL_SERVER, header.meta);
            default:
                return KIO::WorkerResult::fail(KIO::ERR_CANNOT_STAT, header.meta);
            }
            break;
        case 5:
            return KIO::WorkerResult::fail(KIO::ERR_DOES_NOT_EXIST, header.meta);
            break;
        case 6:
            return KIO::WorkerResult::fail(KIO::ERR_ACCESS_DENIED, header.meta);
            break;
        default:
            return KIO::WorkerResult::fail(KIO::ERR_CONNECTION_BROKEN, header.meta);
        }
        disconnectFromHost();
    }
    mRedirectCount = 0;

    if (header.meta.isEmpty()) {
        qCDebug(KIO_GEMINI) << "using default mimeType";
        mimeType(QStringLiteral("text/gemini"));
        setMetaData(QStringLiteral("charset"), QStringLiteral("utf-8"));
    } else {
        auto headerParts = header.meta.split(QLatin1Char(';'));
        for (auto headerPart : headerParts) {
            auto d = headerPart.split(QLatin1Char('='));
            if (d.size() == 2) {
                const auto key = d[0].trimmed().toLower();
                if (QLatin1String("charset") == key) {
                    setMetaData(key, d[1].trimmed());
                } else if (QLatin1String("lang") == key) {
                    setMetaData(QStringLiteral("content-language"),
                                d[1].trimmed()); // same as http kio
                }
            }
        }

        qCDebug(KIO_GEMINI) << "using mimeType" << headerParts[0];
        // set mimeType and send metaData
        mimeType(headerParts[0]);
    }

    return {};
}

#include "gemini.moc"
