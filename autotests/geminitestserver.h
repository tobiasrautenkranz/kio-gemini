#pragma once

#include <QByteArray>
#include <QMutex>
#include <QObject>
#include <QSemaphore>
#include <QThread>
#include <QUrl>
#include <memory>

class SslServer;
class QTcpSocket;

/** A simple Gemini server for client testing. */
class GeminiTestServer : public QThread
{
    Q_OBJECT
public:
    GeminiTestServer();
    ~GeminiTestServer();

    /** Returns the port the server is bound to. */
    quint16 serverPort() const;

    /** Set the response to be send to the client. */
    void setResponse(QByteArray response);
    /** Returns the last request made by the client */
    QByteArray request();

    /** Returns the URL to access the server */
    QUrl root() const;

    /** Sets the number of connections the server accepts before quitting */
    void setMaxConnectionCount(int count);
    /** Returns the number of connections made to the server */
    int connectionCount();

private:
    bool handleClient(QTcpSocket &socket);
    void run() override;

    std::unique_ptr<SslServer> mServer;

    QMutex mMutex;
    QSemaphore mReady;
    quint16 mPort;

    QByteArray mResponse;
    QByteArray mRequest;

    int mMaxConnectionCount = 1;
    int mCurrentConnectionCount = 0;
};
