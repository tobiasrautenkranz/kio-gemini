#pragma once

#include <QTcpServer>

/** A simple SslServer. Mostly a QTcpServer but using a QSslSocket. */
class SslServer : public QTcpServer
{
    Q_OBJECT
public:
    SslServer(QObject *parent = nullptr);

protected:
    void incomingConnection(qintptr socketDescriptor) override;
};
