// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "sslserver.h"

#include <QSslCertificate>
#include <QSslKey>
#include <QSslSocket>

// generated with gemcert for localhost 2021-04-10
Q_GLOBAL_STATIC_WITH_ARGS(const QSslCertificate, defaultCert, (QByteArray(R"(
-----BEGIN CERTIFICATE-----
MIIBSzCB8qADAgECAhEAj07XBLVaHYhyVm0E9HtB+DAKBggqhkjOPQQDAjAUMRIw
EAYDVQQDEwlsb2NhbGhvc3QwHhcNMjEwNDEwMTkwNDQ0WhcNMjYwNDEwMTkwNDQ0
WjAUMRIwEAYDVQQDEwlsb2NhbGhvc3QwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNC
AAT/XDhrC0ZY2GcT2tbcbs/EIyyDh1HUIpUbo7d7dyaBLg3GX3eBmKuLOSOda3yD
oOtejY9pPQWjBIGMEZxug02LoyUwIzAhBgNVHREEGjAYgglsb2NhbGhvc3SCCyou
bG9jYWxob3N0MAoGCCqGSM49BAMCA0gAMEUCIDJg/NccYiooh0R/7+awJq3lpQ0j
PEiomzVS/9t2sMCmAiEAx9cVn+gGuVtDPJHbcRPe2eqvVC3b4c6jCCVlEyPzoJ4=
-----END CERTIFICATE-----
)")))

Q_GLOBAL_STATIC_WITH_ARGS(const QSslKey,
                          defaultKey,
                          (QByteArray(R"(
-----BEGIN PRIVATE KEY-----
MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgy8iLqrE2F5t16a1m
O/CHYO6gGOLYNfKxjVI/hHyALDKhRANCAAT/XDhrC0ZY2GcT2tbcbs/EIyyDh1HU
IpUbo7d7dyaBLg3GX3eBmKuLOSOda3yDoOtejY9pPQWjBIGMEZxug02L
-----END PRIVATE KEY-----
)"),
                           QSsl::Ec))

SslServer::SslServer(QObject *parent)
    : QTcpServer(parent)
{
}

void SslServer::incomingConnection(qintptr socketDescriptor)
{
    QSslSocket *socket = new QSslSocket(this);
    if (!socket->setSocketDescriptor(socketDescriptor)) {
        Q_EMIT acceptError(socket->error());
        delete socket;
        return;
    }

    socket->setProtocol(QSsl::TlsV1_3);
    socket->setLocalCertificate(*defaultCert);
    socket->setPrivateKey(*defaultKey);

    socket->startServerEncryption();
    addPendingConnection(socket);
}

#include "moc_sslserver.cpp"
