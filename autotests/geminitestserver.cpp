// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminitestserver.h"

#include "sslserver.h"

#include <QEventLoop>
#include <QMutexLocker>
#include <QTcpSocket>

GeminiTestServer::GeminiTestServer()
    : mResponse(QByteArrayLiteral("20 \r\nGemini Test Server\r\n"))
{
    start(); // autostart thread
    mReady.acquire();
}

GeminiTestServer::~GeminiTestServer()
{
    wait();
}

quint16 GeminiTestServer::serverPort() const
{
    return mPort;
}

void GeminiTestServer::setResponse(QByteArray response)
{
    QMutexLocker locker(&mMutex);
    mResponse = response;
}

QByteArray GeminiTestServer::request()
{
    QMutexLocker locker(&mMutex);
    return mRequest;
}

bool GeminiTestServer::handleClient(QTcpSocket &socket)
{
    if (!socket.waitForReadyRead()) {
        qCritical() << "no bytes to read";
        return false;
    }

    QByteArray buf = socket.readAll();

    QByteArray response;
    {
        QMutexLocker locker(&mMutex);
        mRequest = buf;
        response = mResponse;
    }

    while (response.size() > 0) {
        auto written = socket.write(response);
        if (-1 == written)
            return false;
        response = response.mid(written);
        socket.waitForBytesWritten();
    }
    socket.flush();

    socket.disconnectFromHost();
    if (socket.state() == QAbstractSocket::UnconnectedState || socket.waitForDisconnected()) {
        return true;
    }

    qDebug() << "disconnect error " << socket.error();
    return false;
}

QUrl GeminiTestServer::root() const
{
    return QUrl(QStringLiteral("gemini://localhost:%2/").arg(serverPort()));
}

void GeminiTestServer::run()
{
    mServer.reset(new SslServer());

    if (mServer->listen(QHostAddress::LocalHost, 0)) {
        mPort = mServer->serverPort();
        qDebug() << "listening " << root();

        mReady.release();
        auto numConnections = mMaxConnectionCount;
        for (int i = 0; i < numConnections; i++) {
            mServer->waitForNewConnection(500);
            auto *socket = mServer->nextPendingConnection();
            if (!socket) {
                qCritical() << "no incomming connection " << i;
                return;
            }
            auto success = handleClient(*socket);
            socket->close();

            if (!success) {
                qCritical() << "handle client failed " << i;
                return;
            }

            QMutexLocker locker(&mMutex);
            numConnections = mMaxConnectionCount;
            mCurrentConnectionCount = i + 1;
        }
    }
}

void GeminiTestServer::setMaxConnectionCount(int count)
{
    QMutexLocker locker(&mMutex);
    mMaxConnectionCount = count;
}

int GeminiTestServer::connectionCount()
{
    QMutexLocker locker(&mMutex);
    return mCurrentConnectionCount;
}

#include "moc_geminitestserver.cpp"
